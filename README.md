# AG Teleautomatisierung: Standard Aufgabe
Bearbeiter: Jan Funke

## Aufgabenstellung / Anforderungen
- Client Anwendung erstellen (WEB oder Anwendung)
	- Generative Erzeugung aus DSL (Ecore oder XML mit XSD)
- Zyklische Prozesswert Anzeige
	- Tabellarisch
	- Graphisch
- DSL
	- Angabe von allen notwendigen Eigenschaften
		- Position
		- Farbe
		- Eigenschaften...

## Analyse
- Elemente
	- Physisch
		- Tank
		- Pumpe
		- Ventil
		- (Leitung)
	- Graphisch
		- Panel
		- Colum
		- Row

