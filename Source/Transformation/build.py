#!/usr/bin/env python3
"""build.py: Loads xml and builds website"""

__author__ = "Jan Funke"
__email__ = "dev@funke.re"
__license__ = "GPL"
__status__ = "Development"

import argparse
import xml.etree.ElementTree as ET
import os
import json
from distutils.dir_util import copy_tree
from dominate.tags import *
from string import Template
from time import sleep
import datetime
import pprint
import re

from classes.plant import *
from classes.signal import *
from classes.component import *

def dumper(obj):
    try:
        return obj.toJSON()
    except:
        return obj.__dict__

def main(modelfile, outputfolder):

    # load xml file
    try:
        with open(modelfile) as f:
            xmlstring = f.read()
    except FileNotFoundError as not_found:
        print("Input file %s was not found! Exiting..." % not_found.filename)
        exit()
 
    # Remove the default namespace definition (xmlns="http://some/namespace")
    xmlstring = re.sub('\\sxmlns="[^"]+"', '', xmlstring, count=1)
 
    # Parse the XML string
    model_tree = ET.ElementTree(ET.fromstring(xmlstring))

    model_root = model_tree.getroot()

    #get basic data
    plant_name = model_root.find('name').text
    plant_location = model_root.find('location').text
    plant = Plant(plant_name, plant_location)

    # backend settings
    backend_url = model_root.find('backend').find('url').text
    backend_protocol = model_root.find('backend').find('protocol').text
    plant.add_backend(backend_url, backend_protocol)

    # url if possible
    webcam_node = model_root.find('webcamURL')
    if (webcam_node != None):
        plant.add_webcam(webcam_node.text)

    # process signals
    plant = process_signals(model_root.find("signals"), plant)

    # process components
    plant = process_components(model_root.find("components"), plant)
     
    # generate html page
    build_page(plant, outputfolder)

def process_signals(signal_node, plant):
    # process signals
    for signal in signal_node.findall("signal"):
        sig_name = signal.get("name")
        sig_address = signal.get("address")
        sig_access = signal.get("access")
        sig_type = signal.get("type")

        sig_desc_node = signal.find('description')
        sig_description = None
        if (sig_desc_node != None):
            sig_description = sig_desc_node.text.replace('\n', '').replace('\t', '').replace('\r', '')
        else:
            sig_description = "NA"

        sig_min_node = signal.find('min')
        sig_min = None
        if (sig_min_node != None):
            sig_min = sig_min_node.text
        else:
            sig_min = "-"
        
        sig_max_node = signal.find('max')
        sig_max = None
        if (sig_max_node != None):
            sig_max = sig_max_node.text
        else:
            sig_max = "-"

        sig_fraction_node = signal.find('fractionDigits')
        sig_fraction = None
        if (sig_fraction_node != None):
            sig_fraction = sig_fraction_node.text
        else:
            sig_fraction = "NA"

        new_sig = Signal(sig_name, sig_address, sig_type, sig_access, sig_description, sig_min, sig_max, sig_fraction)
        plant.add_signal(new_sig)

    return plant

def process_components(components_node, plant): 
    # helper
    max_height = 0
    max_width = 0

    for component in components_node:
        # name and id
        c_name = component.get("name")
        c_id = component.get("id")

        # position
        c_start_cell = component.find("style").find("startCell").text
        c_end_cell = component.find("style").find("endCell").text

        # increase grid size if neccessary
        xy = c_end_cell.split(':')
        max_width = int(xy[0]) if int(xy[0]) > max_width else max_width
        max_height = int(xy[1]) if int(xy[1]) > max_height else max_height

        # type specific data
        data = component.find("data")
        if (component.tag == "tank"):
            t_high_level = plant.get_signal(data.find("highLevel").text) if (data.find("highLevel") != None) else None
            t_low_level = plant.get_signal(data.find("lowLevel").text) if (data.find("lowLevel") != None) else None
            t_level = plant.get_signal(data.find("level").text) if (data.find("level") != None) else None
            t_motor_state = plant.get_signal(data.find("motorState").text) if (data.find("motorState") != None) else None
            t_heater_state = plant.get_signal(data.find("heaterState").text) if (data.find("heaterState") != None) else None

            plant.add_component(Tank(
                c_name,
                c_id,
                c_start_cell,
                c_end_cell,
                t_high_level,
                t_low_level,
                t_level,
                t_motor_state,
                t_heater_state
            ))

        elif (component.tag == "valve"):
            v_valve_state = plant.get_signal(data.find("valveState").text) if (data.find("valveState") != None) else None
            v_valve_hand_target = plant.get_signal(data.find("valveHandTarget").text) if (data.find("valveHandTarget") != None) else None

            plant.add_component(Valve(
                c_name,
                c_id,
                c_start_cell,
                c_end_cell,
                v_valve_state,
                v_valve_hand_target
            ))
        elif (component.tag == "pump"):
            p_pump_state = plant.get_signal(data.find("pumpState").text) if (data.find("pumpState") != None) else None
            p_pump_target = plant.get_signal(data.find("pumpTarget").text) if (data.find("pumpTarget") != None) else None

            plant.add_component(Pump(
                c_name,
                c_id,
                c_start_cell,
                c_end_cell,
                p_pump_state,
                p_pump_target
            ))

    # set dimensions
    plant.set_dimensions(max_width, max_height)

    return plant

def build_page(plant, dst):
    """Builds the webpage with templates and parsed xml data"""

    # build overview
    content_grid = div(id="overview_grid")
    css_areas = ""

    # build grid matrix
    matrix = [['.' for i in range(plant.grid_height)] for j in range(plant.grid_width)]

    # process components
    for component in plant.components:
        # generate entry in html
        content_grid.add(div(component.gen_html(), id="comp-" + component._id, cls="Component " + type(component).__name__))

        # generate css grid area entry
        c_area = "#comp-" + component._id + " {\n" \
            + "\tgrid-area: " + component._id + "; \n } \n"

        css_areas += c_area

        # add to matrix
        start = component.start_cell.split(':')
        end = component.end_cell.split(':')
        for i in range(int(start[0])-1, int(end[0])):
            for j in range(int(start[1])-1, int(end[1])):
                matrix[i][j]=component._id

    # build datatable
    data_table = table(cls='table is-striped is-hoverable is-fullwidth')
    tab_head = data_table.add(thead())
    row_head = tab_head.add(tr())
    row_head.add(th('Signal'))
    row_head.add(th('Address'))
    row_head.add(th('Access'))
    row_head.add(th('Description'))
    row_head.add(th('Min'))
    row_head.add(th('Max'))
    row_head.add(th('Value'))
    tab_body = data_table.add(tbody())

    for signal in plant.signals.values():
        t_row = tab_body.add(tr())
        t_row.add(td(signal.name))
        t_row.add(td(signal.address))
        t_row.add(td(signal.access))
        t_row.add(td(signal.description))
        t_row.add(td(signal.min_val))
        t_row.add(td(signal.max_val))
        t_row.add(th(span('NA', id=signal.name, cls="opc_value")))

    #
    # ─── PREPARE BUILDFOLDER ────────────────────────────────────────────────────────
    #  
    copy_tree("assets", dst)


    #
    # ─── GENERATE CONFIG JSON ───────────────────────────────────────────────────────
    #
    
    json_str = json.dumps(plant.get_signals(),
                            indent=4, sort_keys=True,
                            separators=(',', ': '), ensure_ascii=False,
                            default=dumper)

    json_file = open(dst + "js/signals.json", "w+")
    json_file.seek(0)                       
    json_file.truncate()
    json_file.write(json_str)
    json_file.close()

    #
    # ─── GENERATE CSS ───────────────────────────────────────────────────────────────
    #

    # generate grid container
    css = "#overview_grid {\n" \
        + "\tdisplay: grid;\n" \
        + "\theight: auto;\n" \
        + "\tgrid-gap: 10px;\n" \
        + "\tgrid-template-columns: repeat(" + str(plant.grid_width) + ", 1fr);\n" \
        + "\tgrid-template-rows: repeat(" + str(plant.grid_height) + ", auto);\n"
        
    # generate grid-template-areas
    grid_template_area = "\tgrid-template-areas: \n"
    for i in range(0, plant.grid_height):
        tmp = "\t\t\""
        for j in range(0, plant.grid_width):
            tmp += matrix[j][i] + "\t"
        tmp = tmp[:-1]
        tmp += "\"\n"
        grid_template_area += tmp
    css += grid_template_area + "}\n"

    # add ares
    css += css_areas

    #
    # ─── HMTL FILE GENERATION ───────────────────────────────────────────────────────
    #

    # fill html template
    html_file = open(dst + "index.html", "r+")
    raw_html_template = Template(html_file.read())

    final_html_str = raw_html_template.substitute(ph_overview=content_grid,
        ph_table=data_table,
        name=plant.name,
        location=plant.location,
        webcam = plant.get_webcam_html(),
        generated = "Page was generated " + datetime.datetime.now().strftime("%I:%M%p on %B %d, %Y")
    )
    
    html_file.seek(0)                       
    html_file.truncate()
    html_file.write(final_html_str)
    html_file.close()

    #
    # ─── CSS FILE GENERATION ────────────────────────────────────────────────────────
    #
       
    # fill css template
    css_file = open(dst + "css/main.css", "r+")
    raw_css_template = Template(css_file.read())

    final_css_str = raw_css_template.substitute(ph_css=css)

    css_file.seek(0)                       
    css_file.truncate()
    css_file.write(final_css_str)
    css_file.close()

if __name__ == "__main__":
    # Setup Parser
    parser = argparse.ArgumentParser(description="Website Builder for Modelfiles")
    parser.add_argument('modelfile', metavar="modelfile", type=str, nargs='?', default="Model.xml", help="path to modelfile")
    parser.add_argument('--out', metavar="folder", type=str, nargs='?', default="build", help="path to outputfolder", dest="outputfolder")
    args = parser.parse_args()

    main(args.modelfile, args.outputfolder)
