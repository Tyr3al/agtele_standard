
function process(signalList){

	var readValues = false;
	var signalList = signalList;
	var signalListSoap = '';
	var signalListString = [];
	var read;
	var callbackFunction;
	var currentData;
	var lastResponse;
	
	var readValuesPeriodically = function(){
		if(readValues == true){
			sendSoapReadMessage();
			if(callbackFunction){
				callbackFunction(currentData, lastResponse);
			}
			setTimeout(readValuesPeriodically, 500);
		}
	};
	
	// URL of the proxy that is embedded into the tomcat and thus runs on localhost
	var proxyUrl = 'http://localhost:3000/proxy';
	// URL of the OPC XML DA server that delivers the values
	var xmldaUrl = 'http://141.30.154.211:8087/OPC/DA';
	
	// generate signal list for soap message
	for (signal in signalList) {
		signalListSoap += '<m:Items ItemName="Schneider/' + signalList[signal].name + '"/>\n';
		signalListString.push(signalList[signal].name);
	}

	// returns the SOAP message that can be used to read the process values
	var getSoapReadMessage = function(){
		var soapMessage = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" ' +
			'                   xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" ' +
			'                   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' + 
			'                   xmlns:xsd="http://www.w3.org/2001/XMLSchema">' +
			'  <SOAP-ENV:Body>' +
			'    <m:Read xmlns:m="http://opcfoundation.org/webservices/XMLDA/1.0/">' +
			'      <m:Options ReturnErrorText="false" ReturnDiagnosticInfo="false" ReturnItemTime="false" ReturnItemPath="false" ReturnItemName="true"/>' +
			'      <m:ItemList>' +
					signalListSoap+
			'      </m:ItemList>' +
			'    </m:Read>' +
			'  </SOAP-ENV:Body>' +
			'</SOAP-ENV:Envelope>';
		return soapMessage;
	}

	// evaluates the read response sent by the OPC XML DA server
	var getDataFromReadResponse = function(response){
		var data = {};
		var retItems = response.getElementsByTagName('Items');
		
		for (var i = 0; i < retItems.length; i++) {
			if (retItems[i].firstChild) {
				data[signalListString[i]] = retItems[i].firstChild.firstChild.nodeValue; 
			} else {
				data[signalListString[i]] = 'NF';
				console.warn('Signal ' + signalListString[i] + ' not found on OPCUA Server!');
			}
		}
		
		currentData = data;
		lastResponse = response.getElementsByTagName("ReadResult")[0].getAttribute("ReplyTime");
	};
	
	// sends the SOAP read message via Ajax
	var sendSoapReadMessage = function(){
		//call of the jQuery ajax function ( $.ajax(...) )
		$.ajax({
		    url : proxyUrl, 
		    type: 'post',
			headers: {
				"SOAPAction": '"http://opcfoundation.org/webservices/XMLDA/1.0/Read"',
				"endpoint" : xmldaUrl
			},
		    data: getSoapReadMessage(),
		    success: getDataFromReadResponse,
			error: function (response) {
				console.log("error");
			}
		});
	};
		
	return {
		init : function(){ /* nothing to be done */ },
		setCallback : function(cb){
			callbackFunction = cb;
		},
		setValues : function(source, target){
			sendSoapWriteMessage(source, target);
		},
		startSimulation: function(){
			readValues = true;
			readValuesPeriodically();
		},
		stopSimulation: function(){
			readValue = false;
		}
	};
}