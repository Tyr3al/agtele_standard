// create a new process with visualization data
var myProcess;
var signals;

// tab management
$('#tabs').tabs({
    activate: function(event, ui) {
		console.log("Switched Tab");
		ui.oldTab.removeClass('is-active');
		ui.newTab.addClass('is-active');
    }
});

// show / hide credits modal
function credits() {
	console.log("Showing Credits!");

	var creditsModal = $("#credits");

	if (creditsModal.hasClass("is-active")) {
		creditsModal.removeClass("is-active");
	} else {
		creditsModal.addClass("is-active");
	}
}


// init function load signal json and start
function init() {
	$.getJSON("js/signals.json", function(data) {
		console.log("Loaded Signallist: ", data);
		signals = data;
		myProcess = new process(data);
		initViz();
	})
}

// This method initializes the visualization.
function initViz(){
	//decorateTanks();

	// initialize the process and start the simulation (sets start values)
	myProcess.init();
	myProcess.startSimulation();
	
	// initialize the trend
	//initTrend();
	startViz();
}

// This method is used to reflect process data changes.
function updateViz(data, responseTime){
	console.log("Received Server Update: ", data);
	
	// update information
	for (signal in data) {

		// update components
		var textObj = $("[signal=" + signal + "]").children(".var");

		if (textObj.length > 0) { 
			var signalObj = signals[signal];

			switch (signalObj.var_type.toLowerCase()){
				case "bool":
					var true_text = textObj.parent().attr('true');
					var false_text = textObj.parent().attr('false');

					if (typeof true_text === "undefined" || typeof true_text === "undefined" ) {
						true_text = "True";
						false_text = "False";
					}
					textObj.text((data[signal] == "true") ? true_text : false_text);	
					break;
				case "real":
					var floatVal = parseFloat(data[signal]);
					floatVal = floatVal.toFixed(parseInt(signalObj.fraction));
					textObj.text(floatVal);
				default:
					textObj.text(data[signal]);
			}
		}
		// update table
		jQuery("#" + signal).text(data[signal]);
	}

	// set update time
	jQuery("#last_update").text(responseTime);
}

// This method starts the visualization by setting the callback function.
function startViz(){ 
	myProcess.setCallback(updateViz);
}

// This method stops the visualization by removing the callback function.
function stopViz(){
	myProcess.setCallback(null);
	myProcess.stopSimulation();
}