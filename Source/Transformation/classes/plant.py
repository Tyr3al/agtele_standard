from dominate.tags import *

class Plant:
    """ Plant class is a representation of xml data """
    def __init__(self, name, location):
        self.name = name
        self.location = location
        self.backend_url = None
        self.backend_protocol = None
        self.webcam_url = None
        self.signals = {}
        self.components = []
        self.grid_width = 0
        self.grid_height = 0

    def add_webcam(self, url):
        """adds webcam url"""
        self.webcam_url = url

    def add_backend(self, url, protocol):
        """adds databackend"""
        self.backend_url = url
        self.backend_protocol = protocol

    def add_signal(self, signal):
        """adds signal to array"""
        self.signals[signal.name] = signal

    def get_signal(self, signal_name):
        if signal_name in self.signals:
            return self.signals[signal_name]
        else:
            return None
    
    def get_signals(self):
        return self.signals

    def add_component(self, component):
        """adds components to array"""
        self.components.append(component)

    def set_dimensions(self, x, y):
        """sets dimensions of grid"""
        self.grid_height = y
        self.grid_width = x

    def get_webcam_html(self):
        html = ""
        if (self.webcam_url != None):
            card = div(cls="card")
            header_ = card.add(header(cls="card-header").add(p(cls="card-header-title")))
            header_.add(span(cls="icon is-small").add(i(cls="fas fa-video")))
            header_.add(span("Webcam"))
            card.add(div(cls="card-content").add(div(cls="content").add(embed(type="application/x-vlc-plugin",
                pluginspage="http://www.videolan.org",
                target=self.webcam_url,
                id="vlc"))))

            html = card;    
        return html