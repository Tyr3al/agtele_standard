class Signal:
    """ Signal class is a representation of signals in xml file """
    def __init__(self, name, address, var_type, access, description, min_val, max_val, fraction):
        self.name = name
        self.address = address
        self.access = access
        self.description = description
        self.min_val = min_val
        self.max_val = max_val
        self.fraction = fraction
        self.var_type = var_type