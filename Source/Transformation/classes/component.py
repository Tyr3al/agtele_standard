from dominate.tags import *

class Component:
    """ Component class is a representation of components in xml file """
    def __init__(self, _name, _id, start_cell, end_cell):
        self._name = _name
        self._id = _id
        self.start_cell = start_cell
        self.end_cell = end_cell


class Tank(Component):
    """ Derived tank class is a representation of tank component in xml file """
    def __init__(self, _name, _id, start_cell, end_cell, high_level, low_level, level, motor_state, heater_state):
        Component.__init__(self, _name, _id, start_cell, end_cell)
        self.high_level = high_level
        self.low_level = low_level
        self.level = level
        self.motor_state = motor_state
        self.heater_state = heater_state
    
    def gen_html(self):
        root = article(cls=" message is-info")
        root.add(div(p("Tank: " + self._id), cls="message-header"))
        box = root.add(div(img(src="img/tank.svg", alt=self._name, width="auto"), cls="message-body"))
        if (self.level != None):
            text = box.add(span(cls="value_display", signal=self.level.name))
            text.add(b("Level:"))
            text.add(div('NA', cls="var"))
        if (self.motor_state != None):
            text = box.add(span(cls="value_display", signal=self.motor_state.name, true="On", false="Off"))
            text.add(b("Motor:"))
            text.add(div('NA', cls="var"))
        if (self.heater_state != None):
            text = box.add(span(cls="value_display", signal=self.heater_state.name, true="On", false="Off"))
            text.add(b("Heater:"))
            text.add(div('NA', cls="var"))
        return root


class Valve(Component):
    """ Derived valve class is a representation of tank component in xml file """
    def __init__(self, _name, _id, start_cell, end_cell, valve_state, valve_hand_target):
        Component.__init__(self,  _name, _id, start_cell, end_cell)
        self.valve_state = valve_state
        self.valve_hand_target = valve_hand_target

    def gen_html(self):
        root = article(cls=" message")
        root.add(div(p("Valve: " + self._id), cls="message-header"))
        box = root.add(div(img(src="img/valve.svg", alt=self._name), cls="message-body"))
        if (self.valve_state != None):
            text = box.add(span(cls="value_display", signal=self.valve_state.name, true="Open", false="Closed"))
            text.add(b("Status:"))
            text.add(div('NA', cls="var"))

        return root

class Pump(Component):
    """ Derived pump class is a representation of tank component in xml file """
    def __init__(self, _name, _id, start_cell, end_cell, pump_state, pump_target):
        Component.__init__(self,  _name, _id, start_cell, end_cell)
        self.pump_state = pump_state
        self.pump_target = pump_target

    def gen_html(self):
        root = article(cls=" message is-primary")
        root.add(div(p("Pump: " + self._id), cls="message-header"))
        box = root.add(div(img(src="img/pump.svg", alt=self._name), cls="message-body"))
        if (self.pump_state != None):
            text = box.add(span(cls="value_display", signal=self.pump_state.name, true="On", false="Off"))
            text.add(b("Status:"))
            text.add(div('NA', cls="var"))
        if (self.pump_target != None):
            text = box.add(span(cls="value_display", signal=self.pump_target.name))
            text.add(b("Target:"))
            text.add(div('NA', cls="var"))
        return root