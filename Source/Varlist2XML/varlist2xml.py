#!/usr/bin/env python
"""varlist2xml.py: Loads list of variables and transforms them to xml"""

__author__ = "Jan Funke"
__email__ = "dev@funke.re"
__license__ = "GPL"
__status__ = "Development"

import csv
from lxml import etree
from collections import OrderedDict

signals = etree.Element("signals")
doc = etree.ElementTree(signals)


with open('varlist.csv', newline='') as csvfile:
     spamreader = csv.reader(csvfile, delimiter=';')
     for row in spamreader:
        signal = etree.SubElement(signals, "signal", OrderedDict([("name", row[0]), ("type", row[1]), ("address", row[2]), ("access", row[6])]))
        etree.SubElement(signal, "description").text = row[3]

        # optional arguments
        if (row[4] != ""):
            etree.SubElement(signal, "min").text = row[4]
        if (row[5] != ""):
            etree.SubElement(signal, "max").text = row[5]
        if (row[7] != ""):
            etree.SubElement(signal, "fractionDigits").text = row[7]

with open('./signals.xml', 'wb') as f:
    f.write(etree.tostring(doc, pretty_print=True))