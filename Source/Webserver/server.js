var express = require('express');
var request = require('request');
var app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

app.use('/', express.static('public'));

app.use('/proxy', function(req, res) {  
  req.pipe(request(req.headers.endpoint)).pipe(res);
});

app.listen(3000, function () {
    console.log('Webserver started on port 3000!');
  });
  